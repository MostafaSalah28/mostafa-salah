import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import {ListPage} from '../list/list';
import { DetailsProvider } from '../../providers/details/details';
import { Details} from '../../shared/details' ;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  
  detailsArray : Details [] ;
  errMess : string ;

  constructor(public navCtrl: NavController ,private detailsService : DetailsProvider)
  {
  }

  ngOnInit()
  {
    this.detailsService.getDetails()
    .subscribe(details => this.detailsArray = details , errmess => this.errMess = <any>errmess );
    
  } 

  changLang(){
    var lang = this.detailsService.getLang();
    if(lang === 'en') {
      this.detailsService.setLang('ar');
    }
    else {
      this.detailsService.setLang('en');
    }
    
  }
   

  
}
