import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

import { baseURL} from '../../shared/baseurl';
import { ProcessHttpMsgProvider } from '../process-http-msg/process-http-msg';
import { Details } from '../../shared/details';


import {TranslateService} from '@ngx-translate/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/*
  Generated class for the DetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DetailsProvider {

  private appLang = 'en';

  constructor(private translate: TranslateService , public http: Http, private processHttpMsg : ProcessHttpMsgProvider ) {
    console.log('Hello DetailsProvider Provider');
  }

  getDetails(): Observable< Details [] >
  {
    return this.http.get(baseURL).map(res => { return this.processHttpMsg.extractData(res); })
                                 .catch(error => { return this.processHttpMsg.handleError(error); });
  }

 
  
      setLang(lang) {
          this.appLang = lang;
          this.translate.use(this.appLang);
      }
  
      getLang() {
          return this.appLang;
      }
    }