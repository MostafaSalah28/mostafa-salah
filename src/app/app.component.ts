import { Component, ViewChild ,OnInit } from '@angular/core';
import { Nav, Platform  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DetailsProvider } from '../providers/details/details';
import {TranslateService} from '@ngx-translate/core';

@Component({
  templateUrl: 'app.html'
})
export class MyApp  { 
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  appLang : string;
  pages: Array<{title: string, component: any}>;

  constructor(private translate: TranslateService ,private detailsService : DetailsProvider , public platform: Platform, public statusBar: StatusBar,
             public splashScreen: SplashScreen )
  {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }


  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.appLang = this.detailsService.getLang();
      this.translate.setDefaultLang(this.appLang);
      this.translate.use(this.appLang);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
