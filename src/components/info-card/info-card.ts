import { Component,Input } from '@angular/core';
import { DetailsProvider } from '../../providers/details/details';
/**
 * Generated class for the InfoCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'info-card',
  templateUrl: 'info-card.html'
})
export class InfoCardComponent {

 @Input() cardData ;

  constructor(private detailsService : DetailsProvider) {
  }

}
