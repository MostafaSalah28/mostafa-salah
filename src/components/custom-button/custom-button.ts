import { Component,Input , OnInit} from '@angular/core';

/**
 * Generated class for the CustomButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-button',
  templateUrl: 'custom-button.html'
})
export class CustomButtonComponent implements OnInit{

  @Input() buttonText : string ;
  @Input() iconName : string ;

  constructor() {
  }
ngOnInit() {
  console.log(this.iconName);
}
}
