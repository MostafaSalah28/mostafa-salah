import { NgModule } from '@angular/core';
import { InfoCardComponent } from './info-card/info-card';
import { CustomButtonComponent } from './custom-button/custom-button';
import { TabsBarComponent } from './tabs-bar/tabs-bar';
@NgModule({
	declarations: [InfoCardComponent,
    CustomButtonComponent,
    TabsBarComponent],
	imports: [],
	exports: [InfoCardComponent,
    CustomButtonComponent,
    TabsBarComponent]
})
export class ComponentsModule {}
